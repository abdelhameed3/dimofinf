/*global $ */
/*eslint-env browser*/


//======================== Loading =========================//
$(document).ready(function () {
  "use strict";
  $(window).on('load', function () {
      $(".load").fadeOut(100, function () {
          $("body").css("overflow", "auto");
          $(this).fadeOut(100, function () {
              $(this).remove();
          });
      });
  });

});
//==================== Slider ==============================//
$(document).ready(function () {
    'use strict';
    $('.sliderHome .slider').slick({
        arrows: false,
        dots: true,
        rtl: document.dir === "rtl" ? true : false,
        autoplay: true,
        autoplaySpeed: 5000,
    });

    $('#blog-slider .widget-body').slick({
        arrows: true,
        nextArrow: '.slider-control .prev',
        prevArrow: '.slider-control .next',
        dots: true,
        rtl: document.dir === "rtl" ? true : false,
        autoplay: false,
        slidesToShow: 3,
        slidesToScroll: 2,
        draggable: false,
        responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 2,
                infinite: true,
                dots: true
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
          ]
    });
    const navHeight = $('nav').height();
    const topNavHeight = $('.top-nav').height();
    $('.sliderHome .slider .item').css('height', `calc(100vh - ${navHeight + topNavHeight}px)`)
});
//====================== LightGallary ======================//
lightGallery(document.getElementById('blog-slider'), {
    selector: '.item-tumb'
}); 
//======================== Check All =======================//
$(document).ready(function () {
    'use strict';
    $("#checkAll").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });
    $('input:checkbox').click(function () {
        $("#checkAll").not(this).prop('checked', false);
    });
});
//=================== Add class Active =====================//
$(document).ready(function () {
  $('.custom-widget .custom-dropdown span').click(function (){
    $('.custom-widget .custom-dropdown ul').toggleClass('active');
  });
  
});
//========================Scroll To Top=====================//
$(document).ready(function () {
    var scrollButton = $("#scroll");
    scrollButton.click(function () {
        $("html,body").animate({ scrollTop: 0 }, 900);
    });
});

